//
//  Client.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation

public struct Client {
  public var clientID: String
  public var clientSecret: String
  
  public init(fromID clientID: String, andSecret clientSecret: String) {
    self.clientID = clientID
    self.clientSecret = clientSecret
  }
}
