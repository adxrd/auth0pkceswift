//
//  LoginTokenResponse.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation

/// Parse the response obtained from a token request
class LoginTokenResponse {
  public var accessToken: String?
  private let ACCESS_TOKEN_KEY = "access_token"
  
  public var tokenType: String?
  private let TOKEN_TYPE_KEY = "token_type"
  
  public var refreshToken: String?
  private let REFRESH_TOKEN_KEY = "refresh_token"
  
  public var error: String?
  private let ERROR_KEY = "error"
  
  public var errorDescription: String?
  private let ERROR_DESCRIPTION_KEY = "error_description"
  
  /// Initialize the response from the http response data
  ///
  /// - Parameter data: the http response data
  init(fromResponseData data: Data) {
    // json from data
    guard let json = json(fromResponseData: data) else {
      error = "Invalid response format"
      errorDescription = "JSON could be decoded"
      return
    }
    
    // search each keys in the json
    accessToken = json[ACCESS_TOKEN_KEY] as? String
    tokenType = json[TOKEN_TYPE_KEY] as? String
    refreshToken = json[REFRESH_TOKEN_KEY] as? String
    error = json[ERROR_KEY] as? String
    errorDescription = json[ERROR_DESCRIPTION_KEY] as? String
  }
  
  /// Retrieve the json structure as a map of string from the http response data
  ///
  /// - Parameter data: the http response data
  /// - Returns: the contained json information
  private func json(fromResponseData data: Data) -> [String: Any]? {
    guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else {
      return nil
    }
    return jsonData as? [String: Any]
  }
}
