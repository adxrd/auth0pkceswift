//
//  PKCEAuthorizationResponse.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation

/// Parse the Authentication server's response obtained from
/// The first server response during PKCE flow
class PKCEAuthorizationResponse {
  public var code: String?
  private let CODE_KEY = "code"
  
  public var error: String?
  private let ERROR_KEY = "error"
  
  public var errorDescription: String?
  private let ERROR_DESCRIPTION_KEY = "error_description"
  
  /// Initialize the response from the Apple event obtained
  /// from redirect URI call
  ///
  /// - Parameter event: the apple event
  public init(withEvent event:NSAppleEventDescriptor) {
    guard let url = url(fromEvent: event) else {
      return
    }
    
    // retrieve url components
    let components = URLComponents(url: url, resolvingAgainstBaseURL:false)
    guard let queryParameters = components?.queryItems else {
      return
    }
    
    // get response properties from query parameters
    for param in queryParameters {
      if (param.name == CODE_KEY) {
        code = param.value
      }
      if (param.name == ERROR_KEY) {
        error = param.value
      }
      if (param.name == ERROR_DESCRIPTION_KEY) {
        errorDescription = param.value
      }
    }
  }
  
  /// The url parameter contained in the apple event
  ///
  /// - Parameter event: the apple event
  /// - Returns: the url call that generated the input event
  private func url(fromEvent event: NSAppleEventDescriptor) -> URL? {
    guard let urlString =
      event.paramDescriptor(forKeyword: keyDirectObject)?.stringValue else {
        return nil
    }
    
    return URL(string: urlString)
  }
}
