//
//  ViewController.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Cocoa
import WebKit

class ViewController: NSViewController, WKNavigationDelegate {
  @IBOutlet weak var loginWebView: WKWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loginWebView.navigationDelegate = self
    let codeVerifier = makeCodeVerifier()
    let verifier = PKCEVerifier(codeVerifier: codeVerifier,
                                codeChallenge: makeCodeChallenge(verifier: codeVerifier))
    
    LoginClient.shared.login(
      client: Client(fromID: "YOUR CLIENT ID",
                     andSecret: "YOUR CLIENT SECRET"),
      redirectURI: URL(string: "artlogictest://auth/callback")!,
      verifyWith: verifier,
      displayOn: loginWebView,
      onError: onLoginError) {
                  (accessToken, tokenType) in
                  print(accessToken)
                  print(tokenType)
                  // show login success and start the app !
                  exit(0)
    }
  }
  
  func webView(_ webView: WKWebView,
               didFailProvisionalNavigation navigation: WKNavigation!,
               withError error: Error) {
    // We shouldn't handle error that comes from the processing of our custom URL
    guard let errorURL = (error as NSError).userInfo["NSErrorFailingURLStringKey"] else {
      onLoginError(error)
      return
    }
    guard let scheme = URL(string: errorURL as! String)?.scheme else {
      onLoginError(error)
      return
    }
    if (scheme == "artlogictest") {
      return
    }
    
    // any other type is handled as usual
    onLoginError(error)
  }
  
  func onLoginError(_ error: Error) {
    let errorString = error.localizedDescription
    
    // Show error message
    let alert = NSAlert()
    alert.messageText = "Loading webpage failed"
    alert.informativeText = errorString
    alert.alertStyle = .warning
    alert.runModal()
  }
}

