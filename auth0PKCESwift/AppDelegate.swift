//
//  AppDelegate.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



  func applicationDidFinishLaunching(_ aNotification: Notification) {
    // This is necessary to handle the redirect made by the oauth protocol
    NSAppleEventManager.shared().setEventHandler(
      self,
      andSelector: #selector(AppDelegate.handleEvent(_:withReplyEvent:)),
      forEventClass: AEEventClass(kInternetEventClass),
      andEventID: AEEventID(kAEGetURL)
    )
  }
  
  @objc func handleEvent(_ event: NSAppleEventDescriptor!, withReplyEvent: NSAppleEventDescriptor!) {
    LoginClient.shared.onResponse(event)
  }
  
  func applicationWillTerminate(_ aNotification: Notification) {
  }


}

