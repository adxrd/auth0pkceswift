//
//  LoginClient.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation
import WebKit

/// Error specific to LoginClients
///
/// - urlGenerationError: Returned when the parameters provided generated an invalid URL call
/// - authorizationError: Returned when the provided user couldn't be authorized
/// - emptyResponseError: Returned when the authentication server gave an invalid response
public enum LoginClientError: Error {
  case urlGenerationError
  case authorizationError(error: String?, description: String?)
  case emptyResponseError
}

class LoginClient : NSObject {
  static let shared = LoginClient()
  
  public func login(client: Client,
                    redirectURI: URL,
                    verifyWith verifier: PKCEVerifier,
                    displayOn view:WKWebView,
                    onError errorHandler:@escaping (Error) -> Void,
                    completionHandler: @escaping (String, String) -> Void) {
    self.client = client
    self.redirectURI = redirectURI
    self.verifier = verifier
    self.completionHandler = completionHandler
    self.errorHandler = errorHandler
    
    guard let url = authorizeURL else {
      errorHandler(LoginClientError.urlGenerationError)
      return
    }
    // execute the load request
    view.load(URLRequest(url: url))
  }
  
  /// Called by the view to inform about the success or failure of the process
  /// This function is called by the AppDelegate when the URI event is received
  ///
  /// - Parameter event: the event descriptor that contains all the informations
  ///                    about the URI call
  public func onResponse(_ event: NSAppleEventDescriptor) {
    // handle event
    let resp = PKCEAuthorizationResponse(withEvent: event)
    guard let code = resp.code else {
      self.errorHandler(
        LoginClientError.authorizationError(
          error: resp.error,
          description: resp.errorDescription
      ))
      return
    }
    
    queryToken(withCode: code)
  }
  
  /// Asynchronously query the token using the code sent by the server
  /// Calls the errorHandler or completionHandler to notify success or failure
  ///
  /// - Parameter code: The code obtained by the server
  private func queryToken(withCode code: String) {
    // make the request
    var request = URLRequest(url: ACCESS_TOKEN_URL)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "content-type")
    request.httpBody = makeAccessTokenRequestBody(withCode: code)
    
    // execute it
    let task = URLSession.shared.dataTask(with: request) {
      (optData, response, err) in
      if (err != nil) {
        self.errorHandler(err!)
        return
      }
      
      guard let data = optData else {
        self.errorHandler(LoginClientError.emptyResponseError)
        return
      }
      
      let resp = LoginTokenResponse(fromResponseData: data)
      // set information from response
      let accessToken = resp.accessToken
      let tokenType = resp.tokenType
      
      // raise an error if necessary
      if (resp.error != nil || resp.errorDescription != nil) {
        self.errorHandler(
          LoginClientError.authorizationError(
            error: resp.error,
            description: resp.errorDescription
        ))
        return
      }
      
      if (accessToken != nil && tokenType != nil) {
        self.completionHandler(accessToken!, tokenType!)
        return
      }
      self.errorHandler(LoginClientError.authorizationError(
        error: "Invalid response format",
        description: "Couldn't find access_token and token_type fields"
      ))
    }
    task.resume()
  }
  
  /// Make the authentication request's body.
  ///
  /// - Parameter code: The code obtained by the server
  /// - Returns: The Data structure. Nil if an error happenned
  private func makeAccessTokenRequestBody(withCode code: String) -> Data? {
    var mapData = [String: String]()
    mapData["grant_type"] = "authorization_code"
    mapData["client_id"] = client.clientID
    mapData["code_verifier"] = verifier.codeVerifier
    mapData["code"] = code
    mapData["redirect_uri"] = redirectURI.absoluteString
    
    do {
      return try JSONSerialization.data(withJSONObject: mapData)
    } catch {
      return nil
    }
  }
  
  private var authorizeURL: URL? {
    get {
      var urlComponents = URLComponents()
      urlComponents.scheme = AUTHORIZE_SCHEME
      urlComponents.host = AUTHORIZE_HOST
      urlComponents.path = AUTHORIZE_PATH
      urlComponents.queryItems = [
        URLQueryItem(name: "audience", value: AUDIENCE),
        URLQueryItem(name: "response_type", value: RESPONSE_TYPE),
        URLQueryItem(name: "client_id", value: client.clientID),
        URLQueryItem(name: "code_challenge", value: verifier.codeChallenge),
        URLQueryItem(name: "code_challenge_method", value: CODE_CHALLENGE_METHOD),
        URLQueryItem(name: "redirect_uri", value: redirectURI.absoluteString),
        URLQueryItem(name: "scope", value: SCOPE),
      ]
      return urlComponents.url
    }
  }
  
  // Constants
  private let AUDIENCE = "https://*.audionamix.com/"
  private let RESPONSE_TYPE = "code"
  private let CODE_CHALLENGE_METHOD = "S256"
  private let SCOPE = "adx:user audiofile:owner"
  private let ACCESS_TOKEN_URL = URL(string: "https://audionamix.auth0.com/oauth/token")!
  private let AUTHORIZE_SCHEME = "https"
  private let AUTHORIZE_HOST = "audionamix.auth0.com"
  private let AUTHORIZE_PATH = "/authorize"
  
  // private members
  private var errorHandler: (Error) -> Void = { _ in }
  private var completionHandler: (String, String) -> Void = { _, _ in }
  private var client: Client = Client(fromID: "", andSecret: "")
  private var redirectURI: URL = URL(string: "http://test.com/")!
  private var verifier: PKCEVerifier = PKCEVerifier(codeVerifier: "", codeChallenge: "")
  private var view: WKWebView?
}

