//
//  Utils.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation

func makeCodeVerifier() -> String {
  var buffer = [UInt8](repeating: 0, count: 32)
  _ = SecRandomCopyBytes(kSecRandomDefault, buffer.count, &buffer)
  let verifier = Data(bytes: buffer).base64EncodedString()
    .replacingOccurrences(of: "+", with: "-")
    .replacingOccurrences(of: "/", with: "_")
    .replacingOccurrences(of: "=", with: "")
    .trimmingCharacters(in: .whitespaces)
  return verifier
}

func makeCodeChallenge(verifier: String) -> String {
  guard let data = verifier.data(using: .utf8) else { return "" }
  var buffer = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
  data.withUnsafeBytes {
    _ = CC_SHA256($0, CC_LONG(data.count), &buffer)
  }
  let hash = Data(bytes: buffer)
  let challenge = hash.base64EncodedString()
    .replacingOccurrences(of: "+", with: "-")
    .replacingOccurrences(of: "/", with: "_")
    .replacingOccurrences(of: "=", with: "")
    .trimmingCharacters(in: .whitespaces)
  return challenge
}
