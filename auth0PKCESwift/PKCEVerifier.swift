//
//  PKCEVerifier.swift
//  auth0PKCESwift
//
//  Created by gvincke on 21/03/2018.
//  Copyright © 2018 audionamix. All rights reserved.
//

import Foundation

public struct PKCEVerifier {
  public var codeVerifier: String
  public var codeChallenge: String
  
  public init(codeVerifier verifier: String, codeChallenge challenge: String) {
    codeVerifier = verifier
    codeChallenge = challenge
  }
}
